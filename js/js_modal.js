let modalWindow = document.querySelector("#modalWindow");
let modalOverlay = document.querySelector("#overlayAroundModalWindow");
let closeButton = document.querySelector("#closeBtn");
let openButton = document.querySelector("#openBtn");

openButton.addEventListener("click", function () {
    modalWindow.classList.toggle("comeOnPopUp");
    modalOverlay.classList.toggle("comeOnPopUp");
});

closeButton.addEventListener("click", function () {
    modalWindow.classList.toggle("comeOnPopUp");
    modalOverlay.classList.toggle("comeOnPopUp");
});